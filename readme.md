# Task

This should be a very simple Symfony app modeling an audit log that allows for (via a JSON API):
- Creation of an event (timestamp, type, details), where type must be either of "info", "warning", or "error"
- Retrieval of all events with a filter on type (preferably with pagination)

No frontend or anything necessary - just the API endpoints.
You can either use Symfony and any Framework on top of it, like the API Platform.

# Usage

## List

```
curl --request GET \
  --url 'http://venture-leap.local/api/v1/event/?page=1&type=<type:string>' \
  --header 'Content-Type: application/json'
```

## Create

```
curl --request POST \
  --url http://venture-leap.local/api/v1/event/ \
  --header 'Content-Type: application/json' \
  --data '{
    "type": "",
    "details": "Hello World"
}'
```
