<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Event;
use App\Interface\EventRepositoryInterface;
use App\Model\EventModel;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class EventRequestService
{
    public function __construct(
        private EventRepositoryInterface $eventRepository,
        private ValidatorInterface $validator
    ) {}

    public function createEvent(EventModel $eventModel): Event
    {
        $event = new Event();
        $event->setTimestamp(time());
        $event->setType($eventModel->getType());
        $event->setDetails($eventModel->getDetails());

        $errors = $this->validator->validate($event);
        if (\count($errors) > 0) {
            throw new ValidationFailedException("Can't save the event", $errors);
        }

        $this->eventRepository->save($event);

        return $event;
    }
}
