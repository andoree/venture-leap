<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Event;
use App\Interface\EventRepositoryInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class EventPaginatorService
{
    private const PER_PAGE = 5;

    public function __construct(
        private EventRepositoryInterface $eventRepository,
        private UrlGeneratorInterface $router,
    ) {}

    /**
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function paginateEvents(int $page = 1, ?string $type = null): array
    {
        if ($page < 1 || !$this->checkType($type)) {
            throw new \InvalidArgumentException("Page should be 1 or greater or event type is invalid");
        }

        $qb = $this->eventRepository->createQueryBuilder('e')
            ->orderBy('e.timestamp', 'DESC')
            ->setMaxResults(self::PER_PAGE)
            ->setFirstResult(self::PER_PAGE * ($page - 1));

        if ($type) {
            $qb->where('e.type = :type')
                ->setParameter('type', $type);
        }

        $paginator = new Paginator($qb->getQuery());

        $total = $paginator->count();
        $hasMore = (self::PER_PAGE * $page) < $total;

        return array_merge([
            'data' => $paginator->getIterator()->getArrayCopy(),
            'total' => $total,
        ], $this->getLinks($page, $hasMore));
    }

    private function getLinks(int $page, bool $hasMore): array
    {
        $nextUrl = false;
        if ($hasMore) {
            $nextUrl = $this->router->generate(
                'event.index',
                ['page' => $page + 1],
                UrlGeneratorInterface::ABSOLUTE_URL
            );
        }

        $prevUrl = false;
        if ($page > 1) {
            $prevUrl = $this->router->generate('event.index', ['page' => $page - 1], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        return [
            'hasMore' => $hasMore,
            'prevPage' => $prevUrl,
            'nextPage' => $nextUrl,
        ];
    }

    private function checkType(?string $type = null): bool
    {
        return null === $type || in_array($type, Event::EVENT_TYPES, true);
    }
}
