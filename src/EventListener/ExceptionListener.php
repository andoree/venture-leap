<?php

declare(strict_types=1);

namespace App\EventListener;

use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Exception\ValidationFailedException;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $response = new JsonResponse();
        $response->setData([
            'error' => $exception->getMessage(),
        ]);

        switch (true) {
            case $exception instanceof \InvalidArgumentException;
            case $exception instanceof \Error:
                $response->setStatusCode(Response::HTTP_BAD_REQUEST);
                break;
            case $exception instanceof ValidationFailedException:
                $response->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
                break;
            case $exception instanceof EntityNotFoundException:
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                break;
            default:
                $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $event->setResponse($response);
    }
}
