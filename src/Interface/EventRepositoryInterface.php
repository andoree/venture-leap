<?php

declare(strict_types=1);

namespace App\Interface;

use App\Entity\Event;

interface EventRepositoryInterface
{
    public function findByIdOrFail(int $id): Event;
    public function save(Event $event, bool $persist = true): void;
}
