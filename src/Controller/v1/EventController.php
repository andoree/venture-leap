<?php

declare(strict_types=1);

namespace App\Controller\v1;

use App\Model\EventModel;
use App\Service\EventPaginatorService;
use App\Service\EventRequestService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/event")
 */
class EventController extends AbstractController
{
    public function __construct(
        private SerializerInterface $serializer,
        private EventRequestService $eventRequestService,
        private EventPaginatorService $eventPaginatorService
    ) {}

    /**
     * @Route("/", name="event.index", methods={"GET"})
     */
    public function index(Request $request): JsonResponse
    {
        $page = (int) $request->get('page', 1);
        $data = $this->eventPaginatorService->paginateEvents($page, $request->get('type'));

        return $this->json($data);
    }

    /**
     * @Route("/", name="event.create", methods={"POST"})
     */
    public function create(Request $request): JsonResponse
    {
        $eventModel = $this->serializer->deserialize(
            $request->getContent(),
            EventModel::class,
            'json'
        );
        $event = $this->eventRequestService->createEvent($eventModel);

        return $this->json($event, Response::HTTP_CREATED);
    }
}
