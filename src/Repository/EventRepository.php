<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Event;
use App\Interface\EventRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\Persistence\ManagerRegistry;

class EventRepository extends ServiceEntityRepository implements EventRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    /**
     * @throws EntityNotFoundException
     */
    public function findByIdOrFail(int $id): Event
    {
        $event = $this->find($id);

        if (null === $event) {
            throw new EntityNotFoundException(sprintf('Event with ID %d is not found', $id));
        }

        return $event;
    }

    public function save(Event $event, bool $persist = true): void
    {
        $em = $this->getEntityManager();
        if ($persist) {
            $em->persist($event);
        }
        $em->flush();
    }
}
