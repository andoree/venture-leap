<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 */
class Event implements \JsonSerializable
{
    public const EVENT_TYPES = ['info', 'warning', 'error'];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\Choice(choices=Event::EVENT_TYPES, message="Choose a valid event type")
     */
    private ?string $type;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private ?string $details;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $timestamp;

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getDetails(): ?string
    {
        return $this->details;
    }

    public function setDetails(string $details): void
    {
        $this->details = $details;
    }

    public function getTimestamp(): ?int
    {
        return $this->timestamp;
    }

    public function setTimestamp(int $timestamp): void
    {
        $this->timestamp = $timestamp;
    }

    public function jsonSerialize(): array
    {
        return [
            'type' => $this->type,
            'details' => $this->details,
            'timestamp' => $this->timestamp,
        ];
    }
}
