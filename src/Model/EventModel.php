<?php

declare(strict_types=1);

namespace App\Model;

class EventModel
{
    private ?string $type;
    private ?string $details;

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    public function getDetails(): ?string
    {
        return $this->details;
    }

    public function setDetails(?string $details): void
    {
        $this->details = $details;
    }
}
