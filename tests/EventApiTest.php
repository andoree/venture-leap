<?php

namespace App\Tests;

use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class EventApiTest extends WebTestCase
{
    protected function tearDown(): void
    {
        $entityManager = self::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $purger = new ORMPurger($entityManager);
        $purger->purge();

        parent::tearDown();
    }

    public function testIndex(): void
    {
        $client = static::createClient();

        $client->xmlHttpRequest('GET', '/api/v1/event/');
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $answer = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertArrayHasKey('hasMore', $answer);
        $this->assertArrayHasKey('data', $answer);
        $this->assertSame(0, $answer['total']);
        $this->assertFalse($answer['hasMore']);
    }

    public function testCreate(): void
    {
        $type = 'error';
        $details = 'Some Message';

        $client = static::createClient();
        $client->jsonRequest('POST', '/api/v1/event/', [
            'type' => $type,
            'details' => $details,
        ]);
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $answer = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertArrayHasKey('type', $answer);
        $this->assertSame($type, $answer['type']);
        $this->assertArrayHasKey('details', $answer);
        $this->assertSame($details, $answer['details']);
    }

    public function testCreateFail(): void
    {
        $client = static::createClient();
        $client->jsonRequest('POST', '/api/v1/event/', [
            'type' => '',
            'details' => 'Some Error',
        ]);
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
    }
}
